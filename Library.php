<?php

include 'Book.php';

class Library {

    private $books;

    public function addBook($title, $year){
        $this->books[] = new Book($title, $year);
    }

    public function countBook($books) {
        return count($books);
    }

    public function getBooksCountByYear($books, $year){

        $count = 0;

        foreach ($this->books as $book){
            if($book->getYear() == $year){
                $count++;
            }
        }
        return $count;
    }

}